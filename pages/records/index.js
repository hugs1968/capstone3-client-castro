import {Fragment} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

export default function Records() {




  return (


  		<div className="pt-3 mb-5 container">
  		<h3>Records</h3>
  		<Form> 		
  			
  			<Form.Group>
  			<div className="mb-2 input-group">
  				<Button href="/records/new" className="btn btn-success" >Add</Button>
  				<Form.Control type="text" placeholder="Search Record" className="form-control" />
  				<select className="form-control">
		  			<option value="All" selected>All</option>
		  			<option value="Income">Income</option>
		  			<option value="Expenses">Expense</option>
		  		</select>
				</div>
  			</Form.Group>
  			
  		</Form>
  		</div>
  	)
}

