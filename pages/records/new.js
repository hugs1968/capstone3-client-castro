import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import {Row,Col} from 'react-bootstrap'

export default function newRecord() {




  return (


  		<div className="mt-5 pt-4 mb-5 container">
  		  
      <Row className="justify-content-center">
        <Col md={6}>
          <h3>New Record</h3>
          <Card>
            <Card.Header>Record Information</Card.Header>
            <Card.Body>
              <Form>
                <Form.Group>
                  <Form.Label>
                    Category Type:
                  </Form.Label>
                  <select required className="form-control">
                    <option value="true" disabled="">Select Category</option>                    
                    <option value="Income">Income</option>
                    <option value="Expenses">Expense</option>
                  </select>
                </Form.Group>

                <Form.Group>
                  <Form.Label>
                    Category Name:
                  </Form.Label>
                  <select required className="form-control">
                    <option value="true" disabled="">Select Category</option>                                        
                  </select>
                </Form.Group>

                <Form.Group>
                  <Form.Label>
                    Amount:
                  </Form.Label>
                  <Form.Control placeholder="Enter Amount" required type="number" className="form-control" value="0" />
                </Form.Group>
                <Form.Group>
                  <Form.Label>
                    Description:
                  </Form.Label>
                  <Form.Control type="text" placeholder="Enter Description" required className="form-control" />
                </Form.Group>
              </Form>
              <Button type="submit" className="btn btn-primary">Submit</Button>
            </Card.Body>
          </Card>

        </Col>        
      </Row>

      </div>

  	)
}