import {useContext,useEffect,useState,Fragment} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import AppHelper from '../../app_helper.js'


export default function Categories() {


  return (


  		<div className="pt-3 mb-5 container">
  		<h3>Categories</h3>
        <a class="btn btn-success mt-1 mb-3" href="/categories/new">Add</a>
      <Fragment>
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>Category</th>
            <th>Type</th>
          </tr>
        </thead>
      </table>
  		</Fragment>
      </div>

  	)
}
